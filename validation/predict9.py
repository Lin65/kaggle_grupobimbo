import numpy as np
import pandas as pd
import time
import datetime
import xgboost

# Define size limits for each field to save memory
datadir = ''
train_df = pd.read_csv(datadir + 'train_df_final_predict9_basedon7.csv')

#train_df = train_df[train_df.State ==9]
test_df = train_df[train_df.Semana ==9]
train_df = train_df[train_df.Semana !=9]

train_df.drop('Semana', axis = 1,inplace = True)
test_df.drop('Semana', axis = 1, inplace = True)

X = train_df.drop('log_demand',axis =1).values
y = train_df.log_demand.values

truth = test_df.log_demand.values
pred_X = test_df.drop('log_demand',axis =1).values
truth = np.expm1(truth)
true_values = [[p] for p in truth]

import ml_metrics as metric

for max_depth in [18,20]:
    for min_child_weight in [1]:
        print 'tuning parameters: ', max_depth, ' ', min_child_weight
        print '       start training ...'        
        # construct DMatrices
        dm_train = xgboost.DMatrix(X, label = y)
        dm_test = xgboost.DMatrix(pred_X)
        # training
        start_time = time.time()
        booster = xgboost.train({'eta':0.1, 'objective':'reg:linear','max_depth':max_depth,'min_child_weight':min_child_weight,'subsample': 1,'colsample_bytree':1,'seed':42}, dm_train, num_boost_round =100 )
        pred = booster.predict(dm_test)        
        end_time = time.time()
        print '       finish training ...'
        time_elapse = end_time - start_time
        print '       running time is ', time_elapse
        pred = np.expm1(pred)
        pred[pred<0] = 0
        pred_for_metrics = [[p] for p in pred]
        print '       RMSLE: ', metric.rmsle(true_values, pred_for_metrics)